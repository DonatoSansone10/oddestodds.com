﻿using OddeStodds.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OddeStodds.Service.Models;

namespace OddeStodds.Service.Implementations
{
    public class OddsService : IOddsService
    {
        private readonly IOddsRepository<Odd> _oddsRep = null;

        private readonly IOddHandlersRepository<OddsHandler> _oddHandlersRep = null;


        public OddsService(IOddsRepository<Odd> Rep, IOddHandlersRepository<OddsHandler> HandlersRep)
        {
            _oddsRep = Rep;
            _oddHandlersRep = HandlersRep;
        }

        public List<Odd> GetAllOdds()
        {
            return _oddsRep.GetAll();
        }

        public void AddOdd(Odd curOdd)
        {
            _oddsRep.Add(curOdd);
        }

        public void UpdateOdd(Odd curOdd)
        {
            _oddsRep.Update(curOdd);
        }

        public void DeleteOdd(Odd curOdd)
        {
            _oddsRep.Delete(curOdd);
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }


        public List<OddsHandler> GetAllHandlers()
        {
            return _oddHandlersRep.GetAll();
        }


        public Odd GetOdd(int ID)
        {
            return _oddsRep.Get(ID);
        }


        public void Publish()
        {
            _oddsRep.Publish();
        }
    }
}
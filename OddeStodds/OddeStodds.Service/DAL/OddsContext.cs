﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using OddeStodds.Service.Models;

namespace OddeStodds.Service.DAL
{
    public class OddsContext : DbContext
    {
        public OddsContext()
        {

        }

        public DbSet<Odd> Odds { get; set; }
        public DbSet<OddsHandler> OddHandlers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
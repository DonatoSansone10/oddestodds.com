﻿using OddeStodds.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddeStodds.Service.DAL
{
    public class OddInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<OddsContext>
    {
        protected override void Seed(OddsContext context)
        {
            var OddHandlers = new List<OddsHandler>
            {
            new OddsHandler{Name="Donato",Surname="Sansone",Username="DonSan10",Password="Don1234" },
            new OddsHandler{Name="Test",Surname="Test",Username="Test1",Password="test1234" },
            };

            OddHandlers.ForEach(s => context.OddHandlers.Add(s));
            context.SaveChanges();

            var Odds = new List<Odd>
            {
            new Odd{HomeTeam="Roma", AwayTeam="Milan", WinValue=1.50M, DrawValue=3.50M, LossValue=4.00M,Published=false },
            new Odd{HomeTeam="Juventus", AwayTeam="Frosinone", WinValue=1.20M, DrawValue=8.50M, LossValue=12.00M,Published=false },
            new Odd{HomeTeam="Carpi", AwayTeam="Napoli", WinValue=6.20M, DrawValue=4.50M, LossValue=1.30M,Published=false },
            };
            Odds.ForEach(s => context.Odds.Add(s));
            context.SaveChanges();
        }
    }
}
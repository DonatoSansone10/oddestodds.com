﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using Ninject;
using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Implementations;
using OddeStodds.Service.Repository;
using OddeStodds.Service.Models;

namespace OddeStodds.Service
{
    public class NinjectBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IOddsService>().To<OddsService>();
            Bind<IOddsRepository<Odd>>().To<OddsRepository>();
            Bind<IOddHandlersRepository<OddsHandler>>().To<OddHandlersRepository>();
        }
    }
}
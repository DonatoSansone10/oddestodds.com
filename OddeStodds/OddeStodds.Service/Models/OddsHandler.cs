﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddeStodds.Service.Models
{
    public class OddsHandler
    {
        public int OddsHandlerID { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public string Password { get; set; } 

    }
}
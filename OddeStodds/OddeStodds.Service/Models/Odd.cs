﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddeStodds.Service.Models
{
    public class Odd
    {
        public int ID { get; set; }

        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }

        public decimal WinValue { get; set; }
        public decimal DrawValue { get; set; }
        public decimal LossValue { get; set; }

        public bool Published { get; set; }

    }
}
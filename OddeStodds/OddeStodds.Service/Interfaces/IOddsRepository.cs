﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddeStodds.Service.Interfaces
{
    public interface IOddsRepository<T>
    {
        bool Add(T entity);
        bool Add(List<T> entity);

        void Update(T entity);

        void Delete(T entity);

        void DeleteAll();

        List<T> GetAll();        

        T Get(int ID);

        void Publish();

    }
}

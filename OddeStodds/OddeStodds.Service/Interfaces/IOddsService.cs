﻿using OddeStodds.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddeStodds.Service.Interfaces
{
    public interface IOddsService
    {
        List<Odd> GetAllOdds();
        Odd GetOdd(int ID);
        void AddOdd(Odd curOdd);

        void UpdateOdd(Odd curOdd);

        void DeleteOdd(Odd curOdd);

        void DeleteAll();

        void Publish();

        List<OddsHandler> GetAllHandlers();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddeStodds.Service.Interfaces
{
    public interface IOddHandlersRepository<T>
    {
        bool Add(T entity);
        bool Add(List<T> entity);

        bool Update(T entity);

        void Delete(T entity);

        void DeleteAll();

        List<T> GetAll();        

    }
}

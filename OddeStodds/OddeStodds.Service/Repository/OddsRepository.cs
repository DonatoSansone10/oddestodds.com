﻿using OddeStodds.Service.DAL;
using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OddeStodds.Service.Repository
{
    public class OddsRepository : IOddsRepository<Odd>
    {
        private OddsContext db = new OddsContext();

        public bool Add(Odd entity)
        {
            try
            {
                db.Odds.Add(entity);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Add(List<Odd> entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Odd entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Odd entity)
        {
            db.Odds.Remove(entity);
            db.SaveChanges();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public List<Odd> GetAll()
        {
            return db.Odds.ToList();
        }


        public Odd Get(int ID)
        {
            return db.Odds.Find(ID);
        }


        public void Publish()
        {
             var odds= db.Odds.ToList();
             odds.ForEach(a => a.Published = true);
            db.SaveChanges();
        }
    }
}
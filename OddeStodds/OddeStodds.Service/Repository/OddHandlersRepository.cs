﻿using OddeStodds.Service.DAL;
using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddeStodds.Service.Repository
{
    public class OddHandlersRepository:IOddHandlersRepository<OddsHandler>
    {
        private OddsContext db = new OddsContext();
        public bool Add(OddsHandler entity)
        {
            throw new NotImplementedException();
        }

        public bool Add(List<OddsHandler> entity)
        {
            throw new NotImplementedException();
        }

        public bool Update(OddsHandler entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(OddsHandler entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public List<OddsHandler> GetAll()
        {
            return db.OddHandlers.ToList();
        }
    }
}
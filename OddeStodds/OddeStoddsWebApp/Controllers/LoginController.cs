﻿using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OddeStoddsWebApp.Controllers
{
    public class LoginController : Controller
    {
        IOddsService _OddsService;

        public LoginController(IOddsService oddService)
        {
            _OddsService = oddService;
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]    
        public ActionResult Login(OddsHandler userIn)
        {
            var user = _OddsService.GetAllHandlers().Where(a => a.Username.Equals(userIn.Username) && a.Password.Equals(userIn.Password)).FirstOrDefault();

            if (user != null)
            {
                Session["OddsHandler"] = user;
                ViewData["Failed"] = null;
                return RedirectToAction("Index", "BackOffice");
            }
            ViewData["Failed"] = "Incorrect Credentials";
            return View();
        }
    }
}
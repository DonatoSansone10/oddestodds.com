﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OddeStodds.Service.DAL;
using OddeStodds.Service.Interfaces;

namespace OddeStoddsWebApp.Controllers
{
    public class HomeController : Controller
    {
        IOddsService _OddsService;

        public HomeController(IOddsService oddService)
        {
            _OddsService = oddService;
        }

        public ActionResult Index()
        {
            var odds = _OddsService.GetAllOdds().Where(x => x.Published == true);

            return View(odds);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OddeStodds.Service.DAL;
using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Models;

namespace OddeStoddsWebApp.Controllers
{
    public class BackOfficeController : Controller
    {
        IOddsService _OddsService;
        // GET: BackOffice
        public BackOfficeController(IOddsService oddService)
        {
            _OddsService = oddService;
        }

        public ActionResult Index()
        {
            if (Session["OddsHandler"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            return View(_OddsService.GetAllOdds());
        }

        public ActionResult publish()
        {
            _OddsService.Publish();
            return View();
        }

        public ActionResult Delete(int ID=0)
        {
            return View(_OddsService.GetOdd(ID));
        }

        [HttpPost,ActionName("Delete")]
        public ActionResult delete_conf(int ID)
        {
            Odd _odDel = _OddsService.GetOdd(ID);
            _OddsService.DeleteOdd(_odDel);
            return RedirectToAction("Index");
        }

        public ActionResult Details(int ID = 0)
        {
            return View(_OddsService.GetOdd(ID));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Create(Odd odd)
        {
            _OddsService.AddOdd(odd);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID = 0)
        {
            return View(_OddsService.GetOdd(ID));
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Odd odd)
        {
            _OddsService.UpdateOdd(odd);
            return RedirectToAction("Index");
        }


    }
}
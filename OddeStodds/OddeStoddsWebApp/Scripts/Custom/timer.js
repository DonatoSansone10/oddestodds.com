﻿(function () {
    // Defining a connection to the server hub.
    var myHub = $.connection.oddsHub;
    // Setting logging to true so that we can see whats happening in the browser console log. [OPTIONAL]
    $.connection.hub.logging = true;
    // Start the hub


    $.connection.hub.start();

    //$.connection.hub.start().done(function () {
    //    myHub.server.getListOfOdds();
    //});
   


    myHub.client.updateOddsList = function (oddsList) {
        var oddJson = $.parseJSON(oddsList);        
       
        $("#TableOdds > tbody").html("");

        for (var i = 0; i < oddJson.length; i++) {

            $("#TableOdds").find('tbody')
            .append($('<tr>')
            .append($('<td>')
            .text(oddJson[i].HomeTeam)
            )
            .append($('<td>')
            .text(oddJson[i].AwayTeam)
            )
            .append($('<td>')
            .text(oddJson[i].WinValue)
            )
            .append($('<td>')
            .text(oddJson[i].DrawValue)
            )
            .append($('<td>')
            .text(oddJson[i].LossValue)
            )
            )
        }        
    };


    $("#btnPublish").click(function () {
        // Call SignalR hub method
        myHub.server.getListOfOdds();
    });

}());
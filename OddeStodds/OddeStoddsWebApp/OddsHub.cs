﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using OddeStodds.Service.Interfaces;
using OddeStodds.Service.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OddeStodds.Service.DAL;

namespace OddeStoddsWebApp
{
    public class OddsHub : Hub
    {
        private readonly IOddsService _OddsService;
        private OddsContext db = new OddsContext();

        public OddsHub()
        {            
            var taskTimer = Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    string timeNow = DateTime.Now.ToString();
                    //Sending the server time to all the connected clients on the client method SendServerTime()
                    string oddsList = JsonConvert.SerializeObject(db.Odds.Where(x => x.Published == true));
                    Clients.All.updateOddsList(oddsList);
                    //Delaying by 3 seconds.
                    await Task.Delay(3000);
                }
            }, TaskCreationOptions.LongRunning
                );
        }

        public void Hello()
        {
            Clients.All.hello();
        }

        //public OddsHub(IOddsService OddsService)
        //{
        //    _OddsService = OddsService;
        //}

        public void GetAllOdds()
        {            
            string oddsList = JsonConvert.SerializeObject(db.Odds.Where(x => x.Published == true));
            Clients.All.updateOddsList(oddsList);
        }

        public void GetListOfOdds()
        {
            //var odds = _OddsService.GetAllOdds();            
            string oddsList = JsonConvert.SerializeObject(db.Odds.Where(x => x.Published == true));
            Clients.Caller.updateOddsList(oddsList);
        }
    }
}